# 1 vCPU, 3.75 GB of RAM

provider "google" {
  credentials = "/Users/ayuth/.config/gcloud/application_default_credentials.json"
  project = "senior-project-playground"
  region  = "asia-southeast1"
  zone    = "asia-east1-a"
}

resource "google_compute_instance" "instance-with-telegraf-1-metric-10-intervals" {
  name         = "instance-with-telegraf-1-metric-10-intervals"
  machine_type = "n1-standard-1"
  
  min_cpu_platform = "Intel Skylake"

  boot_disk {
    initialize_params {
      image = "centos-7-v20190213"
    }
  }

  network_interface {
    network = "default"

    access_config {
      // Ephemeral IP
    }
  }

  metadata_startup_script = "sudo sh -c 'curl -o- https://gitlab.com/Senior-Project-Playground/telegraf-benchmark/raw/master/expirement/EXP_II_WITH_TELEGRAF_METRICS/scripts/1-install-1-metric.sh | bash'"
}

resource "google_compute_instance" "instance-with-telegraf-10-metrics-10-intervals" {
  name         = "instance-with-telegraf-10-metrics-10-intervals"
  machine_type = "n1-standard-1"
  
  min_cpu_platform = "Intel Skylake"

  boot_disk {
    initialize_params {
      image = "centos-7-v20190213"
    }
  }

  network_interface {
    network = "default"

    access_config {
      // Ephemeral IP
    }
  }

  metadata_startup_script = "sudo sh -c 'curl -o- https://gitlab.com/Senior-Project-Playground/telegraf-benchmark/raw/master/expirement/EXP_II_WITH_TELEGRAF_METRICS/scripts/2-install-10-metrics.sh | bash'"
}
