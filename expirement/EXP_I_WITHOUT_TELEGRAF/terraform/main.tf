provider "google" {
  credentials = "/Users/ayuth/.config/gcloud/application_default_credentials.json"
  project = "senior-project-playground"
  region  = "asia-southeast1"
  zone    = "asia-east1-a" # Tokyo: asia-northeast1-b, asia-southeast1-a


  # region name jp: asia-northeast1
  # region name jp: asia-northeast1-b

  # region name sg: asia-southeast1
  # region zone sg: asia-east1-a
}

resource "google_compute_instance" "default" {
  name         = "instance-without-telegraf"
  machine_type = "n1-standard-1"
  
  min_cpu_platform = "Intel Skylake"

  tags = ["experiment", "telegraf"]

  boot_disk {
    initialize_params {
      image = "centos-7-v20190213"
    }
  }

  network_interface {
    network = "default"

    access_config {
      // Ephemeral IP
    }
  }

  metadata_startup_script = "sudo sh -c 'curl -o- https://gitlab.com/Senior-Project-Playground/telegraf-benchmark/raw/master/expirement/EXP_I_WITHOUT_TELEGRAF/scripts/install.sh | bash'"

  service_account {
    scopes = ["userinfo-email", "compute-ro", "storage-ro"]
  }
}
