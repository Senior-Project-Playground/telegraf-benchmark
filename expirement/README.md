## Spec

InfluxDB: influxdb-1.7.4-1.x86_64 on centos 7


## Sar with timeout

To use sar with timeout by `sar <every n seconds> <n times>`. For example collecting status of cpu every 1 second for 5 times

```bash
[ayuth_mang@centos-instance ~]$ sar 1 5
```

---

# Collecting system information using `sar`

To collecting systems information by using sar we'll use following commands

## CPU Metrics

```bash
[ayuth@centos-instance ~]$ sar -u 1 3
Linux 3.10.0-957.1.3.el7.x86_64 (centos-instance) 	02/21/2019 	_x86_64_	(1 CPU)

07:59:09 AM     CPU     %user     %nice   %system   %iowait    %steal     %idle
07:59:10 AM     all      0.99      0.00      1.98      0.00      0.00     97.03
07:59:11 AM     all      0.00      0.00      0.00      0.00      0.00    100.00
07:59:12 AM     all      0.00      0.00      0.00      0.00      0.00    100.00
Average:        all      0.33      0.00      0.66      0.00      0.00     99.00
```

## Memory Metrics

```bash
[ayuth@centos-instance ~]$ sar -r 1 3
Linux 3.10.0-957.1.3.el7.x86_64 (centos-instance) 	02/21/2019 	_x86_64_	(1 CPU)

07:59:36 AM kbmemfree kbmemused  %memused kbbuffers  kbcached  kbcommit   %commit  kbactive   kbinact   kbdirty
07:59:37 AM    258580    342748     57.00         0    145012    475720     79.11    185672     72064         0
07:59:38 AM    258580    342748     57.00         0    145012    475720     79.11    185676     72064         0
07:59:39 AM    258208    343120     57.06         0    145012    475692     79.11    185684     72064         0
Average:       258456    342872     57.02         0    145012    475711     79.11    185677     72064         0
```

## Disk Metrics

```bash
[ayuth@centos-instance ~]$ sar -d 1 3
Linux 3.10.0-957.1.3.el7.x86_64 (centos-instance) 	02/21/2019 	_x86_64_	(1 CPU)

07:59:58 AM       DEV       tps  rd_sec/s  wr_sec/s  avgrq-sz  avgqu-sz     await     svctm     %util
07:59:59 AM    dev8-0      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00

07:59:59 AM       DEV       tps  rd_sec/s  wr_sec/s  avgrq-sz  avgqu-sz     await     svctm     %util
08:00:00 AM    dev8-0      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00

08:00:00 AM       DEV       tps  rd_sec/s  wr_sec/s  avgrq-sz  avgqu-sz     await     svctm     %util
08:00:01 AM    dev8-0      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00

Average:          DEV       tps  rd_sec/s  wr_sec/s  avgrq-sz  avgqu-sz     await     svctm     %util
Average:       dev8-0      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00
```

## Network Metrics

```bash
[ayuth@centos-instance ~]$ sar -n DEV 1 3
Linux 3.10.0-957.1.3.el7.x86_64 (centos-instance) 	02/21/2019 	_x86_64_	(1 CPU)

08:00:17 AM     IFACE   rxpck/s   txpck/s    rxkB/s    txkB/s   rxcmp/s   txcmp/s  rxmcst/s
08:00:18 AM      eth0      2.00      1.00      0.13      0.10      0.00      0.00      0.00
08:00:18 AM        lo      0.00      0.00      0.00      0.00      0.00      0.00      0.00

08:00:18 AM     IFACE   rxpck/s   txpck/s    rxkB/s    txkB/s   rxcmp/s   txcmp/s  rxmcst/s
08:00:19 AM      eth0      1.00      1.00      0.06      0.18      0.00      0.00      0.00
08:00:19 AM        lo      0.00      0.00      0.00      0.00      0.00      0.00      0.00

08:00:19 AM     IFACE   rxpck/s   txpck/s    rxkB/s    txkB/s   rxcmp/s   txcmp/s  rxmcst/s
08:00:20 AM      eth0      5.05     14.14      0.58     10.35      0.00      0.00      0.00
08:00:20 AM        lo      0.00      0.00      0.00      0.00      0.00      0.00      0.00

Average:        IFACE   rxpck/s   txpck/s    rxkB/s    txkB/s   rxcmp/s   txcmp/s  rxmcst/s
Average:         eth0      2.68      5.35      0.26      3.52      0.00      0.00      0.00
Average:           lo      0.00      0.00      0.00      0.00      0.00      0.00      0.00
```

## COMBIND ABOVE COMMANDS

```bash
[ayuth@centos-instance ~]$ sar -u -r -d -n DEV 1 3
Linux 3.10.0-957.1.3.el7.x86_64 (centos-instance) 	02/21/2019 	_x86_64_	(1 CPU)

08:00:52 AM     CPU     %user     %nice   %system   %iowait    %steal     %idle
08:00:53 AM     all      0.00      0.00      1.00      0.00      0.00     99.00

08:00:52 AM kbmemfree kbmemused  %memused kbbuffers  kbcached  kbcommit   %commit  kbactive   kbinact   kbdirty
08:00:53 AM    258596    342732     57.00         0    145020    475720     79.11    185708     72064        16

08:00:52 AM       DEV       tps  rd_sec/s  wr_sec/s  avgrq-sz  avgqu-sz     await     svctm     %util
08:00:53 AM    dev8-0      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00

08:00:52 AM     IFACE   rxpck/s   txpck/s    rxkB/s    txkB/s   rxcmp/s   txcmp/s  rxmcst/s
08:00:53 AM      eth0      2.00      1.00      0.13      0.10      0.00      0.00      0.00
08:00:53 AM        lo      0.00      0.00      0.00      0.00      0.00      0.00      0.00

08:00:53 AM     CPU     %user     %nice   %system   %iowait    %steal     %idle
08:00:54 AM     all      0.00      0.00      0.00      0.00      0.00    100.00

08:00:53 AM kbmemfree kbmemused  %memused kbbuffers  kbcached  kbcommit   %commit  kbactive   kbinact   kbdirty
08:00:54 AM    258596    342732     57.00         0    145020    475720     79.11    185716     72064        16

08:00:53 AM       DEV       tps  rd_sec/s  wr_sec/s  avgrq-sz  avgqu-sz     await     svctm     %util
08:00:54 AM    dev8-0      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00

08:00:53 AM     IFACE   rxpck/s   txpck/s    rxkB/s    txkB/s   rxcmp/s   txcmp/s  rxmcst/s
08:00:54 AM      eth0      1.00      1.00      0.06      0.18      0.00      0.00      0.00
08:00:54 AM        lo      0.00      0.00      0.00      0.00      0.00      0.00      0.00

08:00:54 AM     CPU     %user     %nice   %system   %iowait    %steal     %idle
08:00:55 AM     all      1.00      0.00      0.00      0.00      0.00     99.00

08:00:54 AM kbmemfree kbmemused  %memused kbbuffers  kbcached  kbcommit   %commit  kbactive   kbinact   kbdirty
08:00:55 AM    258200    343128     57.06         0    145020    475688     79.11    185720     72064        16

08:00:54 AM       DEV       tps  rd_sec/s  wr_sec/s  avgrq-sz  avgqu-sz     await     svctm     %util
08:00:55 AM    dev8-0      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00

08:00:54 AM     IFACE   rxpck/s   txpck/s    rxkB/s    txkB/s   rxcmp/s   txcmp/s  rxmcst/s
08:00:55 AM      eth0      2.00      2.00      0.14      1.92      0.00      0.00      0.00
08:00:55 AM        lo      0.00      0.00      0.00      0.00      0.00      0.00      0.00

Average:        CPU     %user     %nice   %system   %iowait    %steal     %idle
Average:        all      0.33      0.00      0.33      0.00      0.00     99.33

Average:    kbmemfree kbmemused  %memused kbbuffers  kbcached  kbcommit   %commit  kbactive   kbinact   kbdirty
Average:       258464    342864     57.02         0    145020    475709     79.11    185715     72064        16

Average:          DEV       tps  rd_sec/s  wr_sec/s  avgrq-sz  avgqu-sz     await     svctm     %util
Average:       dev8-0      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00

Average:        IFACE   rxpck/s   txpck/s    rxkB/s    txkB/s   rxcmp/s   txcmp/s  rxmcst/s
Average:         eth0      1.67      1.33      0.11      0.73      0.00      0.00      0.00
Average:           lo      0.00      0.00      0.00      0.00      0.00      0.00      0.00
```
