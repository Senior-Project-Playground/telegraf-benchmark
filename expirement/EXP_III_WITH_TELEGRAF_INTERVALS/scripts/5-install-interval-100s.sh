
# Collect system stats without telegraf installed.

###### Bay's Section
## 1. Initial variables.
export EXP_NAME="EXP_III_WITH_TELEGRAF_5_INTERVAL_100s"
export INFLUX_URL="http:\/\/pc-173.calit2.optiputer.net:8086"
export INFLUX_USER="kenadmin"
export INFLUX_PASSWORD="kenpassword"
export TELEGRAF_INTERVAL="100s"
export TELEGRAF_INPUTS="cpu:mem:net:diskio:system"

## 2. Install requirements.
## - telegraf
## - nodejs 
## - sysstat

# telegraf repo
cat <<EOF | sudo tee /etc/yum.repos.d/influxdb.repo
[influxdb]
name = InfluxDB Repository - RHEL \$releasever
baseurl = https://repos.influxdata.com/rhel/\$releasever/\$basearch/stable
enabled = 1
gpgcheck = 1
gpgkey = https://repos.influxdata.com/influxdb.key
EOF
curl -sL https://rpm.nodesource.com/setup_10.x | sudo bash - #nodejs
yum install -y telegraf-1.9.2 nodejs sysstat
systemctl stop telegraf
systemctl stop sysstat # because we're gonna keep stat by ourself

## 3. Rewrite telegraf config
# Generate telegraf inputs script and save to file
sudo sh -c "telegraf --input-filter $TELEGRAF_INPUTS --output-filter influxdb config | tee > /etc/telegraf/telegraf.conf"

# Edit configuration file
sudo sed -i "s/  # urls = \[\"http:\/\/127.0.0.1:8086\"\]/  urls = \[\"${INFLUX_URL}\"\]/" /etc/telegraf/telegraf.conf
sudo sed -i "s/  # username = \"telegraf\"/  username = \"$INFLUX_USER\"/" /etc/telegraf/telegraf.conf
sudo sed -i "s/  # password = \"metricsmetricsmetricsmetrics\"/  password = \"$INFLUX_PASSWORD\"/" /etc/telegraf/telegraf.conf
sudo sed -i "s/  interval = \"10s\"/  interval = \""$TELEGRAF_INTERVAL\""/" /etc/telegraf/telegraf.conf
sudo sed -i "s/  hostname = \"\"/  hostname = \""$TELEGRAF_HOSTNAME"\"/" /etc/telegraf/telegraf.conf

# Start telegraf service
sudo systemctl restart telegraf

###### End Bay's Section

###### Ken's section
curl https://gitlab.com/Senior-Project-Playground/telegraf-benchmark/raw/master/main.sh | bash
