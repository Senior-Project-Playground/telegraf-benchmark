## Metrics

Telegraf's agent collecting intervals are [0.01, 0.1, 1, 10, 100] seconds.


cpu:mem:net:swap:diskio:system

## Before run

Initialize a Terraform working directory

```bash
cd terraform && terraform init
```

## Run expirement script

```bash
terraform apply
```

```bash
yes
```
