# 1 vCPU, 3.75 GB of RAM

provider "google" {
  credentials = "/Users/ayuth/.config/gcloud/application_default_credentials.json"
  project = "senior-project-playground"
  region  = "asia-southeast1"
  zone    = "asia-east1-a"
}

resource "google_compute_instance" "instance-with-telegraf-interval-10ms" {
  name         = "instance-with-telegraf-interval-10ms"
  machine_type = "n1-standard-1"
  
  min_cpu_platform = "Intel Skylake"

  tags = ["experiment", "telegraf"]

  boot_disk {
    initialize_params {
      image = "centos-7-v20190213"
    }
  }

  network_interface {
    network = "default"

    access_config {
      // Ephemeral IP
    }
  }

  metadata_startup_script = "sudo sh -c 'curl -o- https://gitlab.com/Senior-Project-Playground/telegraf-benchmark/raw/master/expirement/EXP_III_WITH_TELEGRAF_INTERVALS/scripts/1-install-interval-10ms.sh | bash'"
}

resource "google_compute_instance" "instance-with-telegraf-interval-100ms" {
  name         = "instance-with-telegraf-interval-100ms"
  machine_type = "n1-standard-1" # 1 vCPU, 3.75 GB of RAM
  
  min_cpu_platform = "Intel Skylake"

  tags = ["experiment", "telegraf"]

  boot_disk {
    initialize_params {
      image = "centos-7-v20190213"
    }
  }

  network_interface {
    network = "default"

    access_config {
      // Ephemeral IP
    }
  }

  metadata_startup_script = "sudo sh -c 'curl -o- https://gitlab.com/Senior-Project-Playground/telegraf-benchmark/raw/master/expirement/EXP_III_WITH_TELEGRAF_INTERVALS/scripts/2-install-interval-100ms.sh | bash'"
}

resource "google_compute_instance" "instance-with-telegraf-interval-1s" {
  name         = "instance-with-telegraf-interval-1s"
  machine_type = "n1-standard-1" # 1 vCPU, 3.75 GB of RAM
  
  min_cpu_platform = "Intel Skylake"

  tags = ["experiment", "telegraf"]

  boot_disk {
    initialize_params {
      image = "centos-7-v20190213"
    }
  }

  network_interface {
    network = "default"

    access_config {
      // Ephemeral IP
    }
  }

  metadata_startup_script = "sudo sh -c 'curl -o- https://gitlab.com/Senior-Project-Playground/telegraf-benchmark/raw/master/expirement/EXP_III_WITH_TELEGRAF_INTERVALS/scripts/3-install-interval-1s.sh | bash'"
}

resource "google_compute_instance" "instance-with-telegraf-interval-10s" {
  name         = "instance-with-telegraf-interval-10s"
  machine_type = "n1-standard-1" # 1 vCPU, 3.75 GB of RAM
  
  min_cpu_platform = "Intel Skylake"

  tags = ["experiment", "telegraf"]

  boot_disk {
    initialize_params {
      image = "centos-7-v20190213"
    }
  }

  network_interface {
    network = "default"

    access_config {
      // Ephemeral IP
    }
  }

  metadata_startup_script = "sudo sh -c 'curl -o- https://gitlab.com/Senior-Project-Playground/telegraf-benchmark/raw/master/expirement/EXP_III_WITH_TELEGRAF_INTERVALS/scripts/4-install-interval-10s.sh | bash'"
}

resource "google_compute_instance" "instance-with-telegraf-interval-100s" {
  name         = "instance-with-telegraf-interval-100s"
  machine_type = "n1-standard-1" # 1 vCPU, 3.75 GB of RAM
  
  min_cpu_platform = "Intel Skylake"

  tags = ["experiment", "telegraf"]

  boot_disk {
    initialize_params {
      image = "centos-7-v20190213"
    }
  }

  network_interface {
    network = "default"

    access_config {
      // Ephemeral IP
    }
  }

  metadata_startup_script = "sudo sh -c 'curl -o- https://gitlab.com/Senior-Project-Playground/telegraf-benchmark/raw/master/expirement/EXP_III_WITH_TELEGRAF_INTERVALS/scripts/5-install-interval-100s.sh | bash'"
}
