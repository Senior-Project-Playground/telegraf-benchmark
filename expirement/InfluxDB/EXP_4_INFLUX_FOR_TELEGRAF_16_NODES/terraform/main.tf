provider "google" {
  credentials = "${pathexpand("~/.config/gcloud/application_default_credentials.json")}"
  project     = "senior-project-playground"
}

resource "google_compute_instance" "instance-influxdb-for-telegraf-16-nodes" {
  name         = "instance-influxdb-for-telegraf-16-nodes"
  zone         = "us-east1-b"
  machine_type = "n1-standard-1"

  min_cpu_platform = "Intel Skylake"

  tags = ["no-firewall-${random_id.firewall.hex}"]

  boot_disk {
    initialize_params {
      image = "centos-7-v20190213"
    }
  }

  network_interface {
    network = "default"

    access_config {
      // Ephemeral IP
    }
  }

  metadata_startup_script = "sudo bash -c 'curl https://gitlab.com/Senior-Project-Playground/telegraf-benchmark/raw/master/exp2.sh | EXP_NAME=EXP_4_INFLUX_FOR_TELEGRAF_16_NODES bash'"
}

# for generate different telegraf instance name that makes we can run multi exp concurrently
# google limited each zone for 8 public ip
resource "random_id" "instance_id-part-1" {
  byte_length = 8
}

resource "google_compute_instance" "instance-telegraf-part-1" {
  name         = "instance-telegraf-node-${count.index}-${random_id.instance_id-part-1.hex}"
  zone         = "us-central1-c"
  count        = 8
  machine_type = "n1-standard-1"

  min_cpu_platform = "Intel Skylake"

  tags = ["no-firewall-${random_id.firewall.hex}"]

  boot_disk {
    initialize_params {
      image = "centos-7-v20190213"
    }
  }

  network_interface {
    network = "default"

    access_config {}
  }

  depends_on = ["google_compute_instance.instance-influxdb-for-telegraf-16-nodes"]

  metadata_startup_script = "sudo bash -c 'curl -o- https://gitlab.com/Senior-Project-Playground/telegraf-benchmark/raw/master/expirement/InfluxDB/EXP_4_INFLUX_FOR_TELEGRAF_16_NODES/scripts/init-telegraf.sh | INFLUX_URL=${google_compute_instance.instance-influxdb-for-telegraf-16-nodes.network_interface.0.access_config.0.nat_ip}:8086 INFLUX_USER=kenadmin INFLUX_PASSWORD=kenpassword bash'"
}

resource "random_id" "instance_id-part-2" {
  byte_length = 8
}

resource "google_compute_instance" "instance-telegraf-part-2" {
  name         = "instance-telegraf-node-${count.index}-${random_id.instance_id-part-2.hex}"
  zone         = "us-west1-b"
  count        = 8
  machine_type = "n1-standard-1"

  min_cpu_platform = "Intel Skylake"

  tags = ["no-firewall-${random_id.firewall.hex}"]

  boot_disk {
    initialize_params {
      image = "centos-7-v20190213"
    }
  }

  network_interface {
    network = "default"

    access_config {}
  }

  depends_on = ["google_compute_instance.instance-influxdb-for-telegraf-16-nodes"]

  metadata_startup_script = "sudo bash -c 'curl -o- https://gitlab.com/Senior-Project-Playground/telegraf-benchmark/raw/master/expirement/InfluxDB/EXP_4_INFLUX_FOR_TELEGRAF_16_NODES/scripts/init-telegraf.sh | INFLUX_URL=${google_compute_instance.instance-influxdb-for-telegraf-16-nodes.network_interface.0.access_config.0.nat_ip}:8086 INFLUX_USER=kenadmin INFLUX_PASSWORD=kenpassword bash'"
}

# generate different firewall name
resource "random_id" "firewall" {
  byte_length = 8
}

resource "google_compute_firewall" "no-firewall" {
  name    = "no-firewall-${random_id.firewall.hex}"
  network = "default"

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["0-65535"]
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["no-firewall-${random_id.firewall.hex}"]
}
