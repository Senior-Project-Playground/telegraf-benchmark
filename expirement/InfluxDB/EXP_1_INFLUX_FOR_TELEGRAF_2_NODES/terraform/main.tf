provider "google" {
  credentials = "${pathexpand("~/.config/gcloud/application_default_credentials.json")}"
  project     = "senior-project-playground"
  region      = "asia-southeast1"
  zone        = "asia-east1-a"
}

resource "google_compute_instance" "instance-influxdb-for-telegraf-2-nodes" {

  name         = "instance-influxdb-for-telegraf-2-nodes"
  machine_type = "n1-standard-1"

  min_cpu_platform = "Intel Skylake"

  tags = ["no-firewall"]

  boot_disk {
    initialize_params {
      image = "centos-7-v20190213"
    }
  }

  network_interface {
    network = "default"

    access_config {
      // Ephemeral IP
    }
  }
  
  metadata_startup_script = "sudo bash -c 'curl https://gitlab.com/Senior-Project-Playground/telegraf-benchmark/raw/master/exp2.sh | EXP_NAME=EXP_1_INFLUX_FOR_TELEGRAF_2_NODES bash'"
}

# for generate different telegraf instance name that makes we can run multi exp concurrently
resource "random_id" "instance_id" {
 byte_length = 8
}

resource "google_compute_instance" "instance-telegraf" {
  count        = 2
  name         = "instance-telegraf-node-${count.index}-${random_id.instance_id.hex}"
  machine_type = "n1-standard-1"

  min_cpu_platform = "Intel Skylake"

  tags = ["no-firewall"]

  boot_disk {
    initialize_params {
      image = "centos-7-v20190213"
    }
  }

  network_interface {
    network = "default"

    access_config {

    }
  }

  depends_on = ["google_compute_instance.instance-influxdb-for-telegraf-2-nodes"] 

  metadata_startup_script = "sudo bash -c 'curl -o- https://gitlab.com/Senior-Project-Playground/telegraf-benchmark/raw/master/expirement/InfluxDB/EXP_1_INFLUX_FOR_TELEGRAF_2_NODES/scripts/init-telegraf.sh | INFLUX_URL=${google_compute_instance.instance-influxdb-for-telegraf-2-nodes.network_interface.0.access_config.0.nat_ip}:8086 INFLUX_USER=kenadmin INFLUX_PASSWORD=kenpassword bash'"
}

resource "google_compute_firewall" "no-firewall" {
  name    = "no-firewall"
  network = "default"

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["0-65535"]
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["no-firewall"]
}
