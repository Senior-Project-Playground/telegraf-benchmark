provider "google" {
  credentials = "${pathexpand("~/.config/gcloud/application_default_credentials.json")}"
  project     = "senior-project-playground"
}

resource "google_compute_instance" "instance-influxdb-baseline" {
  name         = "instance-influxdb-baseline"
  zone         = "asia-east1-a"
  machine_type = "n1-standard-1"

  min_cpu_platform = "Intel Skylake"

  tags = ["no-firewall-${random_id.firewall.hex}"]

  boot_disk {
    initialize_params {
      image = "centos-7-v20190213"
    }
  }

  network_interface {
    network = "default"

    access_config {
      // Ephemeral IP
    }
  }

  metadata_startup_script = "sudo bash -c 'curl https://gitlab.com/Senior-Project-Playground/telegraf-benchmark/raw/master/exp2.sh | EXP_NAME=EXP_0_INFLUX_BASELINE bash'"
}

# for generate different telegraf instance name that makes we can run multi exp concurrently
resource "random_id" "firewall" {
  byte_length = 8
}

resource "google_compute_firewall" "no-firewall" {
  name    = "no-firewall-${random_id.firewall.hex}"
  network = "default"

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["0-65535"]
  }

  source_ranges = ["0.0.0.0/0"]
  name          = "no-firewall-${random_id.firewall.hex}"
}
