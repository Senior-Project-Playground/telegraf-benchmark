#!/bin/bash

# install influxdb
echo "Installing influxdb..."
## install influxdb
cat << EOF | tee /etc/yum.repos.d/influxdb.repo
[influxdb]
name = InfluxDB Repository - RHEL \$releasever
baseurl = https://repos.influxdata.com/rhel/\$releasever/\$basearch/stable
enabled = 1
gpgcheck = 1
gpgkey = https://repos.influxdata.com/influxdb.key
EOF
yum install -y influxdb ;
systemctl start influxdb ;

echo "sleep 30s after start influxdb"
sleep 30s ;# create database telegraf
mkdir -p /var/lib/influxdb/data/telegraf
influx -execute 'CREATE DATABASE telegraf'
influx -execute "CREATE USER kenadmin WITH PASSWORD 'kenpassword' WITH ALL PRIVILEGES" ;
# enable authentication
sed -i "s/# auth-enabled = false/auth-enabled = true/" /etc/influxdb/influxdb.conf ;

# start service
systemctl restart influxdb
## end of install influxdb

yum install -y git nodejs sysstat
git clone https://gitlab.com/Senior-Project-Playground/telegraf-benchmark.git
cd telegraf-benchmark
npm install

chmod +x ./scripts/collect-disk-space-influx.sh
chmod +x ./scripts/collect-disk-network-1s-10mins.sh

# collect disk-space
echo "Collecting current disk-space..."
./scripts/collect-disk-space-influx.sh

# collect disk i/o net
echo "Collecting DiskI/O and Network status (1s 10mins)..."
./scripts/collect-disk-network-1s-10mins.sh

# collect disk-space
echo "Collecting final disk-space..."
./scripts/collect-disk-space-influx.sh

# install dependencies
echo "Start cleaning and emailing process..."
npm run exp2
