import {
	readFile,
	cleanRawData,
	cleanAverageData,
	calculateSD
} from "./utils/data-cleaner";
import { writeJsonToCsv, removeFiles } from "./utils/csv";
import { emailExperimentResult } from "./utils/mailer";
import moment from "moment";
import math from "mathjs";

async function startCollectingExperiment(
	logFile,
	factors = "",
	experimentName
) {
	// format [ [disk_space, directory, prev_inc], ... ]
	const logs = readFile(logFile).map((current, index, arr) => {
		const currentLog = current.split(/\s+/g);
		const SPACE_INDEX = 0;

		let prevInc = 0;
		if (index !== 0) {
			const prevLog = arr[index - 1].split(/\s+/g);
			prevInc = currentLog[SPACE_INDEX] - prevLog[SPACE_INDEX];
		}

		return [...currentLog, prevInc];
	});
	// console.log("----logs----\n", logs);

	const formattedRawData = logs.map(log => {
		return {
			"USED_SPACE (KB)": log[0],
			DIRECTORY: log[1],
			"INC_FROM_PREV (KB)": log[2]
		};
	});
	// console.log("----formattedRawData----\n", formattedRawData);

	const spaceIncrements = logs.map(log => log[2]);
	// console.log("----spaceIncrements----\n", spaceIncrements);

	const formattedSummaryData = [
		{
			"AVG(INC_FROM_PREV) (KB/minute)": math.mean(spaceIncrements),
			"SD(INC_FROM_PREV)": math.std(spaceIncrements)
		}
	];
	// console.log("----formattedSummaryData----\n", formattedSummaryData);

	//write file
	const destination = `${__dirname}/../seed_files`;

	const resultName = `result-${experimentName}`;
	const resultDestination = `${destination}/${resultName}.csv`;
	writeJsonToCsv(formattedRawData, resultDestination);

	const averageResultName = `average-${experimentName}`;
	const averageResultDestination = `${destination}/${averageResultName}.csv`;

	writeJsonToCsv(formattedSummaryData, averageResultDestination);

	const fileDestinations = [resultDestination, averageResultDestination];
	try {
		await emailExperimentResult(experimentName, fileDestinations);
	} catch (error) {
		const errorMessage = error || error.msg;
		console.log(errorMessage);
	}

	removeFiles(fileDestinations);
}

const TELEGRAF_INTERVAL = process.env.TELEGRAF_INTERVAL || "[Not-Detected]";
const TELEGRAF_INPUTS = process.env.TELEGRAF_INPUTS || "[Not-Detected]";
const interval = `${TELEGRAF_INTERVAL}Interval`;
const metrics = `${TELEGRAF_INPUTS.split`:`.length}Metrics`;

const timestamp = `${moment()
	.utcOffset("+0700")
	.format("MMMDo,hh;mm;ssa")}`;
const experimentName = `${
	process.env.EXP_NAME
}-${interval}-${metrics}-${timestamp}`;

// const LOG_FILE = `${__dirname}/../disk-space.log`;
const LOG_FILE = `/tmp/influxdb/log/disk-space.log`;

startCollectingExperiment(LOG_FILE, process.env.FACTORS, experimentName);
