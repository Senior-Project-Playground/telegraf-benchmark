import fs from "fs";
import math from "mathjs";

const RAW_DATA_TIMESTAMP_REGEX = /\d{2}:\d{2}:\d{2}/g;
const AVERAGE_RESULT_REGEX = /Average:/g;

const FACTORS = {
	CPU: {
		MATCH_REGEX: /CPU\s+%user/g,
		HEADER: "CPU USAGE (%)",
		RESULT_INDEX: 3
	},
	MEM: {
		MATCH_REGEX: /kbmemfree\s+kbmemused/g,
		HEADER: "MEMORY USAGE (%)",
		RESULT_INDEX: 4
	},
	DISK: { MATCH_REGEX: /DEV\s+tps/g, HEADER: "DISK I/O (%)", RESULT_INDEX: 10 },
	NET: { MATCH_REGEX: /IFACE\s+rxpck\/s/, HEADER: "NETWORK", RESULT_INDEX: 5 },
	SPACE: { MATCH_REGEX: /Filesystem/g, HEADER: "DISK SPACE", RESULT_INDEX: 1 }
};

const TUPLE_LENGTH = 2;

export const readFile = fileName => {
	return fs.readFileSync(fileName, "utf-8").split`\n`.filter(
		line => line.length !== 0
	);
};

const _prepareSelectedFactors = factors => {
	return factors.split`,`.map(factor => factor.trim().toUpperCase());
};
const _splitAndGrabData = (data, index) => {
	return data.split(/\s+/g)[index];
};
const _formatLogTupleToJson = (data, selectedFactors) => {
	selectedFactors = selectedFactors.slice();

	let tmpJson = {};
	for (let i = 0; i < data.length; i += TUPLE_LENGTH) {
		const datum = data[i];

		for (let j = 0; j < selectedFactors.length; j++) {
			const selectedFactor = selectedFactors[j];
			const factor = FACTORS[selectedFactor];

			if (datum.match(factor.MATCH_REGEX)) {
				selectedFactors.splice(j, 1);
				tmpJson[factor.HEADER] = _splitAndGrabData(
					data[i + 1],
					factor.RESULT_INDEX
				);
				if (selectedFactor === "NET") i++;
			}
		}
	}

	return tmpJson;
};
const _cleanedDataFromFactors = (data, selectedFactors) => {
	let cleanedData = [];

	selectedFactors = _prepareSelectedFactors(selectedFactors);

	const logLength = TUPLE_LENGTH * selectedFactors.length + 1;
	for (let i = 0; i < data.length; i += logLength) {
		const logTuple = data.slice(i, i + logLength);
		const formattedJson = _formatLogTupleToJson(logTuple, selectedFactors);

		cleanedData.push(formattedJson);
	}

	return cleanedData;
};

export const calculateSD = data => {
	const formattedObj = data.reduce((acc, ele) => {
		for (let key in ele) {
			if (!acc[key]) acc[key] = [ele[key]];
			else acc[key].push(ele[key]);
		}
		return acc;
	}, {});

	const sdObj = {};
	for (let key in formattedObj) {
		sdObj[`SD[${key}]`] = math.std(formattedObj[key]);
	}

	return sdObj;
};

export const cleanRawData = (data, selectedFactors) => {
	data = data.filter(datum => datum.match(RAW_DATA_TIMESTAMP_REGEX));

	return _cleanedDataFromFactors(data, selectedFactors);
};

export const cleanAverageData = (data, selectedFactors) => {
	data = data
		.filter(datum => datum.match(AVERAGE_RESULT_REGEX))
		.map(datum => datum.replace(AVERAGE_RESULT_REGEX, "Average :"));

	return _cleanedDataFromFactors(data, selectedFactors);
};
