import nodemailer from "nodemailer";

let transporter = nodemailer.createTransport({
	service: "gmail",
	auth: {
		user: "kenbaypjj@gmail.com",
		pass: "kenbaynaruk"
	}
});

export const emailExperimentResult = (subject, attachments) => {
	return new Promise((resolve, reject) => {
		let mailOptions = {
			to: "kenbaypjj@gmail.com",
			subject,
			html: subject,
			attachments: attachments.map(attachment => ({
				path: attachment
			}))
		};

		transporter.sendMail(mailOptions, (error, { messageId }) => {
			error ? reject(error) : resolve(messageId);
		});
	});
};
