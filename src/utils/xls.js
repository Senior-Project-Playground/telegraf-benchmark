import json2xls from "json2xls";
import fs from "fs";

export const writeJsonToXlsx = (jsons, exportPath = new Date()) => {
	const xls = json2xls(jsons);
	fs.writeFileSync(exportPath, xls, "binary");
};

export const removeXlsxFiles = fileDestinations => {
	for (let i = 0; i < fileDestinations.length; i++)
		fs.unlinkSync(fileDestinations[i]);
};
