import fs from "fs";
import json2csv from "json2csv";
const Json2csvParser = json2csv.Parser;

const _jsonsToCsv = jsons => {
	const fields = Object.keys(jsons[0]);
	const json2csvParser = new Json2csvParser({ fields });
	const csv = json2csvParser.parse(jsons);

	return csv;
};

export const writeJsonToCsv = (jsons, exportPath = new Date()) => {
	const csv = _jsonsToCsv(jsons);
	fs.writeFileSync(exportPath, csv, "utf8");
};

export const removeFiles = fileDestinations => {
	for (let i = 0; i < fileDestinations.length; i++)
		fs.unlinkSync(fileDestinations[i]);
};
