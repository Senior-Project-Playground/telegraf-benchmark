//CONST
const RIGHT_OUTPUT_MATCH_LENGTH = 18;
const OUTPUT_TIMESTAMP_INDEX = 0;
const OUTPUT_CPU_INDEX = 0;

export const cleanOutput = line => {
	const matchLength = (line.match(/\d+/g) || []).length;
	if (matchLength === RIGHT_OUTPUT_MATCH_LENGTH) console.log(line);
};
