import {
	readFile,
	cleanRawData,
	cleanAverageData,
	calculateSD
} from "./utils/data-cleaner";
import { writeJsonToCsv, removeFiles } from "./utils/csv";
import { emailExperimentResult } from "./utils/mailer";
import moment from "moment";

async function startCollectingExperiment(
	logFile,
	factors = "",
	experimentName
) {
	const logs = readFile(logFile);

	//cleanned and mapped data
	const resultData = cleanRawData(logs, factors);
	const sdData = calculateSD(resultData);

	const averageData = [{ ...sdData, ...cleanAverageData(logs, factors)[0] }];
	console.log(averageData);
	//write file
	const destination = `${__dirname}/../seed_files`;

	const resultName = `result-${experimentName}`;
	const resultDestination = `${destination}/${resultName}.csv`;
	writeJsonToCsv(resultData, resultDestination);

	const averageResultName = `average-${experimentName}`;
	const averageResultDestination = `${destination}/${averageResultName}.csv`;
	writeJsonToCsv(averageData, averageResultDestination);

	const fileDestinations = [resultDestination, averageResultDestination];
	try {
		await emailExperimentResult(experimentName, fileDestinations);
	} catch (error) {
		const errorMessage = error || error.msg;
		console.log(errorMessage);
	}

	removeFiles(fileDestinations);
}
const TELEGRAF_INTERVAL = process.env.TELEGRAF_INTERVAL || "[Not-Detected]";
const TELEGRAF_INPUTS = process.env.TELEGRAF_INPUTS || "[Not-Detected]";
const interval = `${TELEGRAF_INTERVAL}Interval`;
const metrics = `${TELEGRAF_INPUTS.split`:`.length}Metrics`;

const timestamp = `${moment()
	.utcOffset("+0900")
	.format("MMMDo,hh;mm;ssa")}`;
const experimentName = `${
	process.env.EXP_NAME
}-${interval}-${metrics}-${timestamp}`;

const LOG_FILE = `${__dirname}/../monitor-stat.log`;

startCollectingExperiment(LOG_FILE, process.env.FACTORS, experimentName);
