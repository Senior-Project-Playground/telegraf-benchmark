#!/bin/bash

# install influxdb
echo "Installing influxdb (Test)..."
curl https://raw.githubusercontent.com/kennaruk/centos-grafana-influx-sandbox/master/influxdb-centos-setup-test.sh | bash

yum install -y git nodejs sysstat
git clone https://gitlab.com/Senior-Project-Playground/telegraf-benchmark.git
cd telegraf-benchmark
npm install

chmod +x ./scripts/collect-disk-space-influx.sh

echo "Sleep for 2s (Waiting for telegraf preparation)"
sleep 2s

# collect disk-space
echo "Collecting current disk-space... (2m)"
./scripts/collect-disk-space-influx.sh
sleep 2m

# install dependencies
echo "Start cleaning and emailing process..."
npm run collect_disk
