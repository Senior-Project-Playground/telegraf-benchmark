#!/bin/bash
cp /etc/localtime /etc/localtime.org
ln -sf  /usr/share/zoneinfo/Asia/Tokyo /etc/localtime
export TZ=Asia/Bangkok

TRIAL_DATABASE_PATH="/var/lib/influxdb/data/telegraf"
mkdir -p $TRIAL_DATABASE_PATH
chmod 777 $TRIAL_DATABASE_PATH

LOG_DIRECTORY="/tmp/influxdb/log"
mkdir -p $LOG_DIRECTORY

LOG_FILE="$LOG_DIRECTORY/disk-space.log"

CRON_COMMAND="* * * * * du -sk  $TRIAL_DATABASE_PATH | grep $TRIAL_DATABASE_PATH >> $LOG_FILE"
echo "$CRON_COMMAND" | crontab -u root -