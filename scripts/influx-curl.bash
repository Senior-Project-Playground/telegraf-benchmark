#!/bin/bash

setDefaultVariables () {
	EXP_NO=0
}

getArgumentsAndSetVariables () {
	for ARGUMENT in "$@"
	do
		KEY=$(echo $ARGUMENT | cut -f1 -d=)
		VALUE=$(echo $ARGUMENT | cut -f2 -d=) 

		echo "KEY=$KEY"

		case "$KEY" in
			--exp|-exp) EXP_NO=$VALUE ;;
		esac
	done
}

setDefaultVariables
getArgumentsAndSetVariables $@

if [ $EXP_NO = 1 ]; then
	echo $(curl https://gitlab.com/Senior-Project-Playground/telegraf-benchmark/raw/master/test.sh)
elif [ $EXP_NO = 2 ]; then
	echo $(curl https://gitlab.com/Senior-Project-Playground/telegraf-benchmark/raw/master/main.sh)
else
	echo "curl nothing"
fi

echo $EXP_NO