#!/bin/bash

# install influxdb
echo "Installing influxdb..."
curl https://raw.githubusercontent.com/kennaruk/centos-grafana-influx-sandbox/master/influxdb-centos-setup.sh | bash

yum install -y git nodejs sysstat
git clone https://gitlab.com/Senior-Project-Playground/telegraf-benchmark.git
cd telegraf-benchmark
npm install

chmod +x ./scripts/collect-disk-space-influx.sh

echo "Sleep for 3m (Waiting for telegraf preparation)"
sleep 3m

# collect disk-space
echo "Collecting current disk-space..."
./scripts/collect-disk-space-influx.sh
sleep 11m

# install dependencies
echo "Start cleaning and emailing process..."
npm run collect_disk
