# Exp1 CPU,MEM,DISK

```
sudo su
```

```
curl https://gitlab.com/Senior-Project-Playground/telegraf-benchmark/raw/master/main.sh | bash
```

# Test

```
curl https://gitlab.com/Senior-Project-Playground/telegraf-benchmark/raw/master/test.sh | bash
```

# Exp2 collect influx

```
curl https://gitlab.com/Senior-Project-Playground/telegraf-benchmark/raw/master/exp2.sh | sudo bash
```

# Exp2 test

```
curl https://gitlab.com/Senior-Project-Playground/telegraf-benchmark/raw/master/exp2-test.sh | sudo bash
```
